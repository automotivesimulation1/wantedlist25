model timeEquation
  Real x;
//initial equation 
//  x*x = 3*time;
equation
  //der(x) = time;
  x = time^2 + (2*time);
end timeEquation;